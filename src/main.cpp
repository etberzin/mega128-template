// Copyright (c) 2023 刻BITTER
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.


#include <Arduino.h>
#include <MAX6675.h>

#include "oled_basic_1306_arduino_wire.h"
#include "oled_basic_v1.h"


constexpr auto OLED_CS = 7;
constexpr auto OLED_DC = 8;

using LowLevel = oled_basic::ArduinoWire1306_12864;
using Oled = oled_basic::OledDriver<LowLevel>;
// using Oled = oled_basic::OledDriver<oled_basic::ArduinoWire1306_12864>;


constexpr auto &TEXT_FONT = oled_basic::ascii_8x16_no_lower_case;

void setup() {
    pinMode(OLED_CS, OUTPUT);
    pinMode(OLED_DC, OUTPUT);


    Wire.begin();

    SPI.begin();
    MAX6675 k_type{8, &SPI};

    Oled::run_init_sequence();

    Oled::put_str("NUM!", TEXT_FONT, 0, 1);
    Oled::put_str("NUM!", TEXT_FONT, 20, 0);

    int8_t num = -100;

    while (1) {
        uint8_t col = Oled::put_signed(num++, oled_basic::big_digit_12x24, 59, 1, oled_basic::write_mode::inverse);
        Oled::clear_trail(59, 1, col, oled_basic::big_digit_12x24, 4);

        uint8_t k_status = k_type.read();
        if(k_status == 0) {
            float temp = k_type.getTemperature();
            int t = static_cast<int>(temp);
            int d = static_cast<int>((temp - t) * 10);
            uint8_t temp_col = Oled::put_unsigned(t, TEXT_FONT, 0, 3);
            temp_col = Oled::put_char('.', TEXT_FONT, temp_col, 3);
            temp_col = Oled::put_unsigned(d, TEXT_FONT, temp_col, 3);
            Oled::clear_trail(0, 3, temp_col, TEXT_FONT, 5);
        }
        
        delay(500);
    }
}

void loop() {}