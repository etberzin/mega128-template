// Copyright (c) 2023 刻BITTER
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.






#include "Arduino.h"
#include "ioxx_common.h"


namespace ioxx {


    using PinToken = uint8_t;


    
    inline auto test_pin(PinToken pin) {
        return digitalRead(pin);
    }


    inline auto getpin(PinToken pin) {
        return digitalRead(pin);
    }

    // TODO: 批量操作

    inline void setpin(PinToken pin) {
        digitalWrite(pin, 1);
    }


    inline void clrpin(PinToken pin) {
        digitalWrite(pin, 0);
    }


    inline void write_pin(PinToken pin, uint8_t level) {
        if (level) {
            digitalWrite(pin, 1);
        }
        else {
            digitalWrite(pin, 0);
        }
    }


    inline void toggle_pin(PinToken pin) {
        digitalWrite(pin, !digitalRead(pin));
    }


    /**
     * @brief 引脚设为推挽输出模式
     *
     * @param pin
     */
    inline void set_pin_out(PinToken pin) {
        pinMode(pin, OUTPUT);
    }


    /**
     * @brief 引脚设为上拉输入模式
     *
     * @param pin
     */
    inline void set_pin_in(PinToken pin) {
        pinMode(pin, INPUT_PULLUP);
    }


    /**
     * @brief 引脚设为输入模式，无上拉、下拉
     *
     * @param pin
     */
    inline void set_pin_in_no_pull(PinToken pin) {
        pinMode(pin, INPUT);
    }

}  // namespace ioxx