// Copyright (c) 2023 刻BITTER
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.


#pragma once

#include <cstddef>
#include <type_traits>


#ifndef _CAT
    #define _CAT(a, b)        a##b
    #define _CAT3(a, b, c)    a##b##c
    #define _CAT4(a, b, c, d) a##b##c##d
#endif


#define _IOXX_ENUM_TO_UNDERLYING(e) static_cast<std::underlying_type_t<decltype(e)>>(e)

#define _IOXX_ENUM_TO_ENUM(e1, e2) static_cast<e2>(static_cast<std::underlying_type_t<decltype(e1)>>(e1))


namespace ioxx {

    enum class cmd {
        disable = 0,
        enable = 1,
    };


    enum class polarity {
        high = 1,
        low = 0,
    };


    enum class level {
        high = 1,
        low = 0,
    };


    template <typename EnumType>
    constexpr auto _calc_enum_underlying_sum(EnumType e) {
        return _IOXX_ENUM_TO_UNDERLYING(e);
    }


    template <typename EnumType, typename... Ts>
    constexpr auto _calc_enum_underlying_sum(EnumType e, Ts... args) {
        return _IOXX_ENUM_TO_UNDERLYING(e) | _calc_enum_underlying_sum(args...);
    }


    template <typename ReturnType>
    constexpr ReturnType _calc_bv_sum(unsigned int bit_position) {
        return static_cast<ReturnType>(1 << bit_position);
    }


    /**
     * @brief 相当于将多个_BV(bit_position) 或起来返回
     * 
     * @tparam ReturnType 
     * @param bit_position 
     * @return constexpr ReturnType 
     */
    template <typename ReturnType, typename... Ts>
    constexpr ReturnType _calc_bv_sum(unsigned int bit_position, Ts... args) {
        return _calc_bv_sum<ReturnType>(bit_position) | _calc_bv_sum<ReturnType>(args...);
    }


}  // namespace ioxx