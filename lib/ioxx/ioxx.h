// Copyright (c) 2023 刻BITTER
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.


/**
 * @file ioxx.h
 * @author 刻bitter (etberzin@foxmail.com)
 * @brief
 * @date 2023-05-05
 *
 *
 * 函数命名规则：
 * 1. init 开头的函数和固件库的init 函数对应，都有相应的链式调用初始化版本
 * 2. setup 开头的函数用于执行重要且不需要变动的设置
 * 3. set 开头的函数与init 函数类似，但没有链式调用版本。set用在所有配置和赋值的函数，包含config 的含义
 */


#pragma once


#include "ioxx_common.h"


#if (defined(_IOXX_ARCH_STM32F10X) || defined(STM32F10x) || defined(STM32F1) || defined(STM32F10x_LD) || defined(STM32F10x_MD) || defined(STM32F10x_HD))

// 依赖: Stm32f10x Std Peripheral Driver 3.5.0
// 可选依赖：Perf Counter

    // clang-format off
    #include "stm32f10x.h"
    #include "ioxx_stm32f10x.h"
    // clang-format on

    #ifndef _IOXX_ARCH_STM32F10X
        #define _IOXX_ARCH_STM32F10X
    #endif

#elif (defined(_IOXX_ARCH_ARDUINO))

    // 依赖Arduino 框架
    // clang-format off
    #include "Arduino.h"
    #include "ioxx_arduino.h"
    // clang-format on

    #ifndef _IOXX_ARCH_ARDUINO
        #define _IOXX_ARCH_ARDUINO
    #endif

#endif
