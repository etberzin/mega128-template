// Copyright (c) 2023 刻BITTER
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.


#pragma once

#include <cstdint>

#include "ioxx.h"


#ifndef _BUTTON_POLL_LONG_PRESS_THRESHOLD
    // 每次按键扫描周期约8ms，所以长按就是大约1s。阈值最大值为254
    #define _BUTTON_POLL_LONG_PRESS_THRESHOLD (120)
#endif


#ifndef _BUTTON_POLL_DOUBLE_PRESS_THRESHOLD
    // 双击事件的阈值时间，一次点击后，在阈值时间内再次按下按键，触发双击事件
    #define _BUTTON_POLL_DOUBLE_PRESS_THRESHOLD (30)
#endif


#ifndef _BUTTON_POLL_FILTER_MASK
    // 连续6 次低电平或高电平对应按键按下或抬起
    #define _BUTTON_POLL_FILTER_MASK (0x3f)
#endif


#ifndef _BUTTON_POLL_RETURN_TYPE
    #define _BUTTON_POLL_RETURN_TYPE void
#endif


#ifndef _BUTTON_POLL_RETURN_VALUE
    // 默认的按键扫描周期是100ms
    #define _BUTTON_POLL_RETURN_VALUE (100)
#endif


// 条件编译开关，关闭按键双击功能，节省代码和内存空间
// #define _BUTTON_POLL_DISABLE_DOUBLE_PRESS


// 条件编译开关，关闭按键多次扫描消抖。如果按键轮询周期比较长，可以去掉消抖
// #define _BUTTON_POLL_DISABLE_SCAN_BUFFER

#include <type_traits>


namespace button_polling {


    union _ButtonFlagByte {
        uint8_t _flag_byte;  // 可以用flag_byte 同时清零所有标志位

       public:
        // 每个成员对应一个标志位，一共8 个标志，刚好放进1 个字节里
        struct {
            uint8_t _pressed : 1;

            // _pressed 和_long_pressed
            // 用于支持扫描函数的内部逻辑，外部不应该直接读取或清零。 带button
            // 前缀的标志位用于传递按键事件，扫描函数只对事件置位，由调用者在响应事件后清零标志位。
            // 除了双击超时事件，每次按键抬起时，双击超时事件都将自动清零
            uint8_t button_pressed : 1;
            uint8_t button_released : 1;
            uint8_t button_clicked : 1;
            uint8_t button_long_pressed : 1;
            uint8_t button_long_press_released : 1;

#ifndef _BUTTON_POLL_DISABLE_DOUBLE_PRESS
            uint8_t button_double_pressed : 1;
            uint8_t button_double_press_timeout : 1;
#endif
        } bits;
    };


    extern _ButtonFlagByte flag;


    inline void clear_all_flag() {
        flag._flag_byte = 0;
    }


    // 集成消抖功能的按键轮询函数

    template <typename RetType = _BUTTON_POLL_RETURN_TYPE, unsigned int RetValue = _BUTTON_POLL_RETURN_VALUE>
    RetType poll_button(ioxx::PinToken pin) {
        static_assert(static_cast<bool>(std::is_same<RetType, void>::value) || (RetValue != 0), "Return value should not be zero.");

        static uint8_t long_press_counter = 0;

#ifndef _BUTTON_POLL_DISABLE_DOUBLE_PRESS
        static uint8_t double_press_counter = 0;
#endif

#ifndef _BUTTON_POLL_DISABLE_SCAN_BUFFER

        static uint8_t scan_buffer = 0xff;  // 按键是低电平有效，所以初始状态缓冲区全为1

        scan_buffer <<= 1;
        if (ioxx::getpin(pin)) {
            scan_buffer |= 0x01;
        }

#endif

        if (!flag.bits._pressed) {
#ifndef _BUTTON_POLL_DISABLE_SCAN_BUFFER
            if ((scan_buffer & _BUTTON_POLL_FILTER_MASK) == 0) {  // 连续6 次低电平，表示按键确实按下了
#else
            if (ioxx::getpin(pin) == 0) {
#endif
                flag.bits._pressed = 1;
                flag.bits.button_pressed = 1;  // 函数外面的标志位由主程序检查，使用一次后清零，以免重复触发

                // 启动长按计数器
                long_press_counter = _BUTTON_POLL_LONG_PRESS_THRESHOLD;

#ifndef _BUTTON_POLL_DISABLE_DOUBLE_PRESS

                // 第二次按下发生在双击等待时间内，触发双击事件
                if (double_press_counter != 0) {
                    flag.bits.button_double_pressed = 1;
                    double_press_counter = 0;
                }
            }
            else {  // 否则按键还在抬起状态，更新双击等待计数器，直到计数器归零
                if (double_press_counter != 0) {
                    --double_press_counter;
                }
#endif
            }
        }
        else {
#ifndef _BUTTON_POLL_DISABLE_SCAN_BUFFER
            if (((~scan_buffer) & _BUTTON_POLL_FILTER_MASK) == 0) {  // 连续6 次高电平，表示按键抬起。所以一次press 至少会持续8 次扫描
#else
            if (ioxx::getpin(pin) == 1) {
#endif
                flag.bits._pressed = 0;

#ifndef _BUTTON_POLL_DISABLE_DOUBLE_PRESS
                // 启动双击等待计数器，同时清零双击超时事件
                double_press_counter = _BUTTON_POLL_DOUBLE_PRESS_THRESHOLD;
                flag.bits.button_double_press_timeout = 0;
#endif

                // 长按后抬起，忽略clicked 事件
                if (long_press_counter == 0) {
                    flag.bits.button_long_press_released = 1;
                }
                else {
                    flag.bits.button_clicked = 1;
                }

                // 清零长按计数器
                long_press_counter = 0;
                flag.bits.button_released = 1;
            }
            else {  //  否则按键尚未抬起，更新长按计数器和长按事件
                if (long_press_counter != 0) {
                    --(long_press_counter);

                    if (long_press_counter == 0) {
                        flag.bits.button_long_pressed = 1;
                    }
                }
            }
        }

        if constexpr (std::is_same<RetType, void>::value) {
            return;
        }
        else {
            return RetValue;
        }
    }

}  // namespace button_polling